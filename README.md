# Service Usage

* AWI webODV

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data

* Number of daily users - This is the number of unique daily users.
* Number of daily users (intern AWI) - This is the number of unique daily users from AWI.
* Number of daily users (extern) - This is the number of unique external (non AWI) daily users.
* Total number of daily ODV instances - This is the number of how many ODV instances have been started at the server at that day.
* Number of daily data imports - This represents the daily imports from the total number of ODV instances.
* Number of daily Explorers - From the total daily ODV instances these are the Explorer sessions.
* Number of daily Extractors - From the total daily ODV instances these are the Extractor sessions.
* Total used storage - The total used storage in GB.

## Schedule

* daily

